-module(riaktest).
-author("Andrzej Maj").


-export([start_client/0, stop_client/1, check_connection/1, put/5, get/3, update/4, delete/3, list_keys/2]).


start_client() ->
  {ok, Pid} = riakc_pb_socket:start_link("127.0.0.1", 8087),
  Pid.

stop_client(Pid) ->
  riakc_pb_socket:stop(Pid),
  {ok, stopped}.


check_connection(Pid) ->
  case riakc_pb_socket:ping(Pid) of
    pong -> {ok, connected};
    _ -> {error, disconnected}
  end.


put(Pid, Bucket, Key, Value, Max) ->
  if Key < Max ->
    BinaryKey = integer_to_binary(Key),
    Object = riakc_obj:new(Bucket, BinaryKey, {Key, Value}),
    riakc_pb_socket:put(Pid, Object),
    put(Pid, Bucket, Key + 1, Value, Max);
    true -> []
  end.


get(Pid, Bucket, Key) ->
  BinaryKey = integer_to_binary(Key),
  {ok, Fetched} = riakc_pb_socket:get(Pid, Bucket, BinaryKey),
  Fetched2 = binary_to_term(riakc_obj:get_value(Fetched)),
  Fetched2.


update(Pid, Bucket, Key, NewValue) ->
  BinaryKey = integer_to_binary(Key),
  {ok, Fetched} = riakc_pb_socket:get(Pid, Bucket, BinaryKey),
  UpdatedObj = riakc_obj:update_value(Fetched, NewValue),
  {ok, NewObject} = riakc_pb_socket:put(Pid, UpdatedObj, [return_body]),
  binary_to_term(riakc_obj:get_value(NewObject)).


delete(Pid, Bucket, Key) ->
  BinaryKey = integer_to_binary(Key),
  riakc_pb_socket:delete(Pid, Bucket, BinaryKey).


list_keys(Pid, Bucket) ->
  riakc_pb_socket:list_keys(Pid, Bucket).
